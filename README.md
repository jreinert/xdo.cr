# xdo

TODO: Write a description here

## Installation

Add this to your application's `shard.yml`:

```yaml
dependencies:
  xdo:
    gitlab: jreinert/xdo
```

## Usage

```crystal
require "xdo"
```

TODO: Write usage instructions here

## Development

TODO: Write development instructions here

## Contributing

1. Fork it (<https://gitlab.com/jreinert/xdo/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [jreinert](https://gitlab.com/jreinert) Joakim Reinert - creator, maintainer
